const int sensor = A0;
const int calibrateButton = A1;

int sensorValue = 0;
int oldValue = 0;
int runningEstimate = 0;
int buttonValue = 0;
int noBeerThreshold = 700;
bool beerFlow = false;
unsigned long startTime;
unsigned long endTime;

void setup() {
  Serial.begin(9600);
  calibrate();
  beerFlow = beerFlowing();
}

void loop() {
  int buttonReading = buttonIsPressed();
  if (buttonReading != buttonValue) {
    buttonValue = buttonReading;
    if (buttonReading) {
      calibrate();
    } 
  } else {
    const bool newBeerFlow = beerFlowing();
    if (!beerFlow && newBeerFlow) {
      startTimer();
      Serial.print("Started a timer\n");
    } else if (beerFlow && !newBeerFlow) {
      endTimer();
      Serial.print("Finished timer with time in milliseconds ");
      Serial.print(endTime - startTime);
      Serial.println("");
    }
    beerFlow = newBeerFlow;
  }
  delay(3);
  
}

void startTimer() {
  startTime = millis();
}

void endTimer() {
  endTime = millis();
}

bool buttonIsPressed() {
  int value = analogRead(calibrateButton);
  Serial.println(value);
  return value > 1000;
}

bool beerFlowing() {
  const int checks = 20;
  int estimate = analogRead(sensor)/checks;
  for (int i = 1; i < checks; i++) {
    estimate = estimate + analogRead(sensor)/checks;
    delay(2);
  }
  
  return estimate < noBeerThreshold; 
}

void calibrate() {
  const int calibrations = 10;
  int estimate = analogRead(sensor)/calibrations;
  for (int i = 1; i < calibrations; i++) {
    estimate = estimate + analogRead(sensor)/calibrations;
    delay(5);
  }
  noBeerThreshold = estimate - 50;
  Serial.println("Did a calibration");
}